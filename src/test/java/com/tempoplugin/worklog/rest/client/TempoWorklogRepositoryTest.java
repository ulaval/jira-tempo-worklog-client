package com.tempoplugin.worklog.rest.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tempoplugin.worklog.rest.client.domain.TempoWorklog;
import com.tempoplugin.worklog.rest.client.domain.TempoWorklogs;
import com.tempoplugin.worklog.rest.client.exception.CommunicationException;
import com.tempoplugin.worklog.rest.client.repository.TempoWorklogRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = JUnitTempoWorklogConfig.class,
        initializers = ConfigFileApplicationContextInitializer.class)
@ActiveProfiles("test")
public class TempoWorklogRepositoryTest {

    @Autowired
    private TempoWorklogRepository worklogRepository;

    @Test
    public void testGetWorklogsByDates() throws CommunicationException {
        Calendar dateDebut = new GregorianCalendar(2014, Calendar.OCTOBER, 01);
        Calendar dateFin = new GregorianCalendar(2014, Calendar.OCTOBER, 07);
        TempoWorklogs test = worklogRepository.getWorklogsByDate(dateDebut.getTime(), dateFin.getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        assertNotNull(test);
        assertNotNull(test.getWorklogs());
        for (TempoWorklog worklog : test.getWorklogs()) {
            try {
                assertTrue((dateFormat.parse(worklog.getWorkDate())).compareTo(dateDebut.getTime()) >= 0);
                assertTrue((dateFormat.parse(worklog.getWorkDate())).compareTo(dateFin.getTime()) <= 0);
            } catch (ParseException e) {
                fail();
            }
        }
    }

    @Test
    public void testGetWorklogsByDatesRetourneAucunWorklog() throws CommunicationException {
        Calendar dateDebut = new GregorianCalendar(2008, Calendar.OCTOBER, 01);
        Calendar dateFin = new GregorianCalendar(2008, Calendar.OCTOBER, 07);
        TempoWorklogs test = worklogRepository.getWorklogsByDate(dateDebut.getTime(), dateFin.getTime());
        assertNotNull(test);
        assertNull(test.getWorklogs());
    }

    @Test
    public void testGetWorklogsByDatesAndProject() throws CommunicationException {
        Calendar dateDebut = new GregorianCalendar(2014, Calendar.OCTOBER, 01);
        Calendar dateFin = new GregorianCalendar(2014, Calendar.OCTOBER, 07);
        TempoWorklogs test = worklogRepository.getWorklogsByDateAndProject(dateDebut.getTime(), dateFin.getTime(), "AC");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        assertNotNull(test);
        for (TempoWorklog worklog : test.getWorklogs()) {
            try {
                assertTrue((dateFormat.parse(worklog.getWorkDate())).compareTo(dateDebut.getTime()) >= 0);
                assertTrue((dateFormat.parse(worklog.getWorkDate())).compareTo(dateFin.getTime()) <= 0);
                assertEquals("AC", worklog.getIssueDetails().getProjectKey());
            } catch (ParseException e) {
                fail();
            }
        }
    }

    @Test
    public void testGetWorklogsByDatesAndIssue() throws CommunicationException {
        Calendar dateDebut = new GregorianCalendar(2014, Calendar.OCTOBER, 01);
        Calendar dateFin = new GregorianCalendar(2014, Calendar.OCTOBER, 07);
        TempoWorklogs test = worklogRepository.getWorklogsByDateAndIssue(dateDebut.getTime(), dateFin.getTime(), "AC-41");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        assertNotNull(test);
        for (TempoWorklog worklog : test.getWorklogs()) {
            try {
                assertTrue((dateFormat.parse(worklog.getWorkDate())).compareTo(dateDebut.getTime()) >= 0);
                assertTrue((dateFormat.parse(worklog.getWorkDate())).compareTo(dateFin.getTime()) <= 0);
                assertEquals("AC-41", worklog.getIssueKey());
            } catch (ParseException e) {
                fail();
            }
        }
    }

    @Test
    public void testGetWorklogsByDatesAndIdul() throws CommunicationException {
        Calendar dateDebut = new GregorianCalendar(2014, Calendar.OCTOBER, 01);
        Calendar dateFin = new GregorianCalendar(2014, Calendar.OCTOBER, 07);
        TempoWorklogs test = worklogRepository.getWorklogsByDateAndIdul(dateDebut.getTime(), dateFin.getTime(), "gaclo32");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        assertNotNull(test);
        for (TempoWorklog worklog : test.getWorklogs()) {
            try {
                assertTrue((dateFormat.parse(worklog.getWorkDate())).compareTo(dateDebut.getTime()) >= 0);
                assertTrue((dateFormat.parse(worklog.getWorkDate())).compareTo(dateFin.getTime()) <= 0);
                assertEquals("gaclo32", worklog.getUsername());
            } catch (ParseException e) {
                fail();
            }
        }
    }
}

package com.tempoplugin.worklog.rest.client;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.tempoplugin.worklog.rest.client.repository.TempoWorklogRepository;

@Configuration
@ComponentScan("com.tempoplugin.worklog.rest.client")
public class JUnitTempoWorklogConfig {

    @Bean
    public TempoWorklogRepository tempoWorklogRepository() {

        //EXP
        return new TempoWorklogRepository("https://jira-exp.ulaval.ca/plugins/servlet/tempo-getWorklog/", "tempoApiToken=42a1b412-fe14-42ad-be46-333b2b8a9cc4");

        //AT
        //        return new TempoWorklogRepository("https://jira.at.ulaval.ca/plugins/servlet/tempo-getWorklog/",
        //                "tempoApiToken=42a1b412-fe14-42ad-be46-333b2b8a9cc4");
    }

}

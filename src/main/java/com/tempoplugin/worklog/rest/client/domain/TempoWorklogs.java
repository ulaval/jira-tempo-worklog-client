package com.tempoplugin.worklog.rest.client.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "dateFrom",
        "dateTo",
        "numberOfWorklogs",
        "format",
        "diffOnly",
        "errorsOnly",
        "validOnly",
        "addBillingInfo",
        "addIssueSummary",
        "addIssueDescription",
        "durationMs",
        "headerOnly",
        "userName",
        "addIssueDetails",
        "addParentIssue",
        "addUserDetails",
        "addWorklogsDetails",
        "billingKey",
        "issueKey",
        "projectKey",
        "addApprovalStatus",
        "worklogs"
})
@XmlRootElement(name = "worklogs")
public class TempoWorklogs {

    @XmlAttribute(name = "date_from")
    private String dateFrom;
    
    @XmlAttribute(name = "date_to")
    private String dateTo;
    
    @XmlAttribute(name = "number_of_worklogs")
    private Integer numberOfWorklogs;
    
    @XmlAttribute(name = "format")
    private String format;
    
    @XmlAttribute(name = "diffOnly")
    private boolean diffOnly;
    
    @XmlAttribute(name = "errorsOnly")
    private boolean errorsOnly;
    
    @XmlAttribute(name = "validOnly")
    private boolean validOnly;
    
    @XmlAttribute(name = "addBillingInfo")
    private boolean addBillingInfo;
    
    @XmlAttribute(name = "addIssueSummary")
    private boolean addIssueSummary;
    
    @XmlAttribute(name = "addIssueDescription")
    private boolean addIssueDescription;
    
    @XmlAttribute(name = "duration_ms")
    private Long durationMs;
    
    @XmlAttribute(name = "headerOnly")
    private boolean headerOnly;
    
    @XmlAttribute(name = "userName")
    private String userName;
    
    @XmlAttribute(name = "addIssueDetails")
    private boolean addIssueDetails;
    
    @XmlAttribute(name = "addParentIssue")
    private boolean addParentIssue;
    
    @XmlAttribute(name = "addUserDetails")
    private boolean addUserDetails;
    
    @XmlAttribute(name = "addWorklogsDetails")
    private boolean addWorklogsDetails;
    
    @XmlAttribute(name = "billingKey")
    private String billingKey;
    
    @XmlAttribute(name = "issueKey")
    private String issueKey;
    
    @XmlAttribute(name = "projectKey")
    private String projectKey;
    
    @XmlAttribute(name = "addApprovalStatus")
    private boolean addApprovalStatus;
    
    @XmlElement(name = "worklog")
    private List<TempoWorklog> worklogs;

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public Integer getNumberOfWorklogs() {
        return numberOfWorklogs;
    }

    public void setNumberOfWorklogs(Integer numberOfWorklogs) {
        this.numberOfWorklogs = numberOfWorklogs;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public boolean isDiffOnly() {
        return diffOnly;
    }

    public void setDiffOnly(boolean diffOnly) {
        this.diffOnly = diffOnly;
    }

    public boolean isErrorsOnly() {
        return errorsOnly;
    }

    public void setErrorsOnly(boolean errorsOnly) {
        this.errorsOnly = errorsOnly;
    }

    public boolean isValidOnly() {
        return validOnly;
    }

    public void setValidOnly(boolean validOnly) {
        this.validOnly = validOnly;
    }

    public boolean isAddBillingInfo() {
        return addBillingInfo;
    }

    public void setAddBillingInfo(boolean addBillingInfo) {
        this.addBillingInfo = addBillingInfo;
    }

    public boolean isAddIssueSummary() {
        return addIssueSummary;
    }

    public void setAddIssueSummary(boolean addIssueSummary) {
        this.addIssueSummary = addIssueSummary;
    }

    public boolean isAddIssueDescription() {
        return addIssueDescription;
    }

    public void setAddIssueDescription(boolean addIssueDescription) {
        this.addIssueDescription = addIssueDescription;
    }

    public Long getDurationMs() {
        return durationMs;
    }

    public void setDurationMs(Long durationMs) {
        this.durationMs = durationMs;
    }

    public boolean isHeaderOnly() {
        return headerOnly;
    }

    public void setHeaderOnly(boolean headerOnly) {
        this.headerOnly = headerOnly;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isAddIssueDetails() {
        return addIssueDetails;
    }

    public void setAddIssueDetails(boolean addIssueDetails) {
        this.addIssueDetails = addIssueDetails;
    }

    public boolean isAddParentIssue() {
        return addParentIssue;
    }

    public void setAddParentIssue(boolean addParentIssue) {
        this.addParentIssue = addParentIssue;
    }

    public boolean isAddUserDetails() {
        return addUserDetails;
    }

    public void setAddUserDetails(boolean addUserDetails) {
        this.addUserDetails = addUserDetails;
    }

    public boolean isAddWorklogsDetails() {
        return addWorklogsDetails;
    }

    public void setAddWorklogsDetails(boolean addWorklogsDetails) {
        this.addWorklogsDetails = addWorklogsDetails;
    }

    public String getBillingKey() {
        return billingKey;
    }

    public void setBillingKey(String billingKey) {
        this.billingKey = billingKey;
    }

    public String getIssueKey() {
        return issueKey;
    }

    public void setIssueKey(String issueKey) {
        this.issueKey = issueKey;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public boolean isAddApprovalStatus() {
        return addApprovalStatus;
    }

    public void setAddApprovalStatus(boolean addApprovalStatus) {
        this.addApprovalStatus = addApprovalStatus;
    }

    
    public List<TempoWorklog> getWorklogs() {
        return worklogs;
    }

    
    public void setWorklogs(List<TempoWorklog> worklogs) {
        this.worklogs = worklogs;
    }

}

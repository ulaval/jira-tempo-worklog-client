package com.tempoplugin.worklog.rest.client.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "fullName",
        "superviseur"
})
@XmlRootElement(name = "user_details")
public class TempoUserDetail {

    @XmlElement(name = "full_name")
    private String fullName;
    
    @XmlElement(name = "user-prop")
    private String superviseur;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    public String getSuperviseur() {
        return superviseur;
    }
    
    public void setSuperviseur(String superviseur) {
        this.superviseur = superviseur;
    }
}

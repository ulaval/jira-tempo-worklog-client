package com.tempoplugin.worklog.rest.client.exception;

/**
 * Exception lancée lorsque la communication avec JIRA est impossible
 * @author isauc4
 *
 */
public class CommunicationException extends Exception {

    private static final long serialVersionUID = 3220192186856901054L;

    public CommunicationException() {
        super();
    }

    public CommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommunicationException(String message) {
        super(message);
    }

    public CommunicationException(Throwable cause) {
        super(cause);
    }
}
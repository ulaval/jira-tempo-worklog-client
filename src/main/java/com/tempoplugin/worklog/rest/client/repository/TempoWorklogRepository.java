package com.tempoplugin.worklog.rest.client.repository;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXB;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.tempoplugin.worklog.rest.client.domain.TempoWorklogs;
import com.tempoplugin.worklog.rest.client.exception.CommunicationException;

public class TempoWorklogRepository {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private final String baseUrl;
    private final String apiToken;

    public TempoWorklogRepository(String baseUrl, String apiToken) {
        this.baseUrl = baseUrl;
        this.apiToken = apiToken;
    }

    public TempoWorklogs getWorklogsByDate(Date dateDebut, Date dateFin) throws CommunicationException {

        TempoWorklogs worklogs = null;
        String url = baseUrl
                + "?dateFrom="
                + dateFormat.format(dateDebut)
                + "&dateTo="
                + dateFormat.format(dateFin)
                + "&format=xml&diffOnly=false&addIssueSummary=true&addIssueDetails=true&addWorklogDetails=true&addUserDetails=true&"
                + apiToken;

        try {
            worklogs = executeHttpGet(url);
        } catch (IOException e) {
            throw new CommunicationException("La communication avec JIRA est impossible", e);
        }

        return worklogs;
    }

    public TempoWorklogs getWorklogsByDateAndProject(Date dateDebut, Date dateFin, String projectKey) throws CommunicationException {

        TempoWorklogs worklogs = null;
        String url = baseUrl
                + "?dateFrom="
                + dateFormat.format(dateDebut)
                + "&dateTo="
                + dateFormat.format(dateFin)
                + "&projectKey="
                + projectKey
                + "&format=xml&addIssueSummary=true&addIssueDetails=true&addWorklogDetails=true&addUserDetails=true&"
                + apiToken;

        try {
            worklogs = executeHttpGet(url);
        } catch (IOException e) {
            throw new CommunicationException("La communication avec JIRA est impossible", e);
        }
        return worklogs;
    }

    public TempoWorklogs getWorklogsByDateAndIssue(Date dateDebut, Date dateFin, String issueKey) throws CommunicationException {

        TempoWorklogs worklogs = null;
        String url = baseUrl
                + "?dateFrom="
                + dateFormat.format(dateDebut)
                + "&dateTo="
                + dateFormat.format(dateFin)
                + "&issueKey="
                + issueKey
                + "&format=xml&addIssueSummary=true&addIssueDetails=true&addWorklogDetails=true&addUserDetails=true&"
                + apiToken;

        try {
            worklogs = executeHttpGet(url);
        } catch (IOException e) {
            throw new CommunicationException("La communication avec JIRA est impossible", e);
        }
        return worklogs;
    }

    public TempoWorklogs getWorklogsByDateAndIdul(Date dateDebut, Date dateFin, String idul) throws CommunicationException {

        TempoWorklogs worklogs = null;
        String url = baseUrl
                + "?dateFrom="
                + dateFormat.format(dateDebut)
                + "&dateTo="
                + dateFormat.format(dateFin)
                + "&userName="
                + idul
                + "&format=xml&addIssueSummary=true&addIssueDetails=true&addWorklogDetails=true&addUserDetails=true&"
                + apiToken;

        try {
            worklogs = executeHttpGet(url);
        } catch (IOException e) {
            throw new CommunicationException("La communication avec JIRA est impossible", e);
        }
        return worklogs;
    }

    private TempoWorklogs executeHttpGet(String url) throws IOException, CommunicationException {

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(new HttpGet(url));
        if (httpResponse.getStatusLine().getStatusCode() != 200) {
            throw new CommunicationException("La communication avec JIRA est impossible. " + "Code erreur : " + httpResponse.getStatusLine().getStatusCode());
        }

        HttpEntity entity = httpResponse.getEntity();

        TempoWorklogs response = JAXB.unmarshal(entity.getContent(), TempoWorklogs.class);
        if (response == null) {
            throw new IOException();
        }
        return response;
    }
}

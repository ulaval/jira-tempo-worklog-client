package com.tempoplugin.worklog.rest.client.domain;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "worklogId",
        "issueId",
        "issueKey",
        "hours",
        "workDate",
        "username",
        "staffId",
        "billingKey",
        "billingAttributes",
        "activityId",
        "activityName",
        "workDescription",
        "parentKey",
        "reporter",
        "externalId",
        "externalTStamp",
        "externalHours",
        "externalResult",
        "issueSummary",
        "hashValue",
        "issueDetails",
        "userDetails",
        "worklogDetail"
})
@XmlRootElement(name = "worklog")
public class TempoWorklog {

    @XmlElement(name = "worklog_id")
    private Long worklogId;

    @XmlElement(name = "issue_id")
    private Long issueId;

    @XmlElement(name = "issue_key")
    private String issueKey;

    @XmlElement(name = "hours")
    private BigDecimal hours;

    @XmlElement(name = "work_date")
    private String workDate;

    @XmlElement(name = "username")
    private String username;

    @XmlElement(name = "staff_id")
    private Long staffId;

    @XmlElement(name = "billing_key")
    private String billingKey;

    @XmlElement(name = "billing_attributes")
    private String billingAttributes;

    @XmlElement(name = "activity_id")
    private Long activityId;

    @XmlElement(name = "activity_name")
    private String activityName;

    @XmlElement(name = "work_description")
    private String workDescription;

    @XmlElement(name = "parent_key")
    private String parentKey;

    @XmlElement(name = "reporter")
    private String reporter;

    @XmlElement(name = "external_id")
    private Long externalId;

    @XmlElement(name = "external_tstamp")
    private String externalTStamp;

    @XmlElement(name = "external_hours")
    private BigDecimal externalHours;

    @XmlElement(name = "external_result")
    private String externalResult;

    @XmlElement(name = "issue_summary")
    private String issueSummary;

    @XmlElement(name = "hash_value")
    private String hashValue;

    @XmlElement(name = "issue_details")
    private TempoIssueDetail issueDetails;

    @XmlElement(name = "user_details")
    private TempoUserDetail userDetails;

    @XmlElement(name = "worklog_details")
    private TempoWorklogDetail worklogDetail;

    public Long getWorklogId() {
        return worklogId;
    }

    public void setWorklogId(Long worklogId) {
        this.worklogId = worklogId;
    }

    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    public String getIssueKey() {
        return issueKey;
    }

    public void setIssueKey(String issueKey) {
        this.issueKey = issueKey;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }

    public String getWorkDate() {
        return workDate;
    }

    public void setWorkDate(String workDate) {
        this.workDate = workDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getBillingKey() {
        return billingKey;
    }

    public void setBillingKey(String billingKey) {
        this.billingKey = billingKey;
    }

    public String getBillingAttributes() {
        return billingAttributes;
    }

    public void setBillingAttributes(String billingAttributes) {
        this.billingAttributes = billingAttributes;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(String workDescription) {
        this.workDescription = workDescription;
    }

    public String getParentKey() {
        return parentKey;
    }

    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public String getExternalTStamp() {
        return externalTStamp;
    }

    public void setExternalTStamp(String externalTStamp) {
        this.externalTStamp = externalTStamp;
    }

    public BigDecimal getExternalHours() {
        return externalHours;
    }

    public void setExternalHours(BigDecimal externalHours) {
        this.externalHours = externalHours;
    }

    public String getExternalResult() {
        return externalResult;
    }

    public void setExternalResult(String externalResult) {
        this.externalResult = externalResult;
    }

    public String getIssueSummary() {
        return issueSummary;
    }

    public void setIssueSummary(String issueSummary) {
        this.issueSummary = issueSummary;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue;
    }

    public TempoIssueDetail getIssueDetails() {
        return issueDetails;
    }

    public void setIssueDetails(TempoIssueDetail issueDetails) {
        this.issueDetails = issueDetails;
    }

    public TempoUserDetail getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(TempoUserDetail userDetails) {
        this.userDetails = userDetails;
    }

    public TempoWorklogDetail getWorklogDetail() {
        return worklogDetail;
    }

    public void setWorklogDetail(TempoWorklogDetail worklogDetail) {
        this.worklogDetail = worklogDetail;
    }

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this.worklogId, ((TempoWorklog)obj).getWorklogId());
	}

}

package com.tempoplugin.worklog.rest.client.domain;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "projectKey",
        "projectName",
        "projectCategory",
        "statusId",
        "statusName",
        "typeId",
        "typeName",
        "remainingEstimate",
        "priorityId",
        "priorityName",
        "created",
        "updated",
        "componentId",
        "componentName"
})
@XmlRootElement(name = "issue_details")
public class TempoIssueDetail {

    @XmlElement(name = "project_key")
    private String projectKey;

    @XmlElement(name = "project_name")
    private String projectName;

    @XmlElement(name = "project_category")
    private String projectCategory;

    @XmlElement(name = "status_id")
    private Long statusId;

    @XmlElement(name = "status_name")
    private String statusName;

    @XmlElement(name = "type_id")
    private Long typeId;

    @XmlElement(name = "type_name")
    private String typeName;

    @XmlElement(name = "remaining_estimate")
    private BigDecimal remainingEstimate;

    @XmlElement(name = "priority_id")
    private Long priorityId;

    @XmlElement(name = "priority_name")
    private String priorityName;

    @XmlElement(name = "created")
    private String created;

    @XmlElement(name = "updated")
    private String updated;

    @XmlElement(name = "component_id")
    private String componentId;

    @XmlElement(name = "component_name")
    private String componentName;

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCategory() {
        return projectCategory;
    }

    public void setProjectCategory(String projectCategory) {
        this.projectCategory = projectCategory;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public BigDecimal getRemainingEstimate() {
        return remainingEstimate;
    }

    public void setRemainingEstimate(BigDecimal remainingEstimate) {
        this.remainingEstimate = remainingEstimate;
    }

    public Long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }

    public String getPriorityName() {
        return priorityName;
    }

    public void setPriorityName(String priorityName) {
        this.priorityName = priorityName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

}
